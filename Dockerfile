# Base image
FROM node:18
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build

CMD [ "node", "dist/main.js" ]

#Test Docker : docker build -t app-rest .

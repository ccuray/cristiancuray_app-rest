import { INestApplication } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';

export class VersioningRedirectMiddleware {
    version: string;
    availableRoutes = [];
    constructor(redirectVersion: string, app: INestApplication) {
        this.version = redirectVersion;
        const server = app.getHttpServer();
        const router = server._events.request._router;
        this.availableRoutes = router.stack
            .map((layer) => {
                if (layer.route) {
                    return {
                        route: {
                            path: layer.route?.path,
                            method: layer.route?.stack[0].method,
                        },
                    };
                }
            })
            .filter((item) => item !== undefined);
    }

    use(req: Request, res: Response, next: NextFunction) {
        let changeURL = false;
        const splitURL = req.url.split('/');
        if (splitURL[1].startsWith('v')) {
            const versionNumber = splitURL[1].replace(/\D/g, '');
            if (!versionNumber) {
                changeURL = true;
            }
        } else {
            changeURL = true;
        }
        if (changeURL) req.url = `/v${this.version + req.url}`;
        next();
    }
}
